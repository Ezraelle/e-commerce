import React from 'react';
import { Link } from 'react-router-dom';
import { Navbar, Nav, NavDropdown, Form, FormControl } from 'react-bootstrap';
import "./NavBar.css";
// import './index.css';
import { IoMdLogIn, IoIosCart } from 'react-icons/io'
import { FaRegRegistered, FaSearch } from 'react-icons/fa'
// import config from "./config.json";


const Navb = () => {
    return (
            <div>
                <Navbar className="border-bottom bg-dark" expand="lg">
                    <Navbar.Brand className="text-white font-weight-bold">
                        <Link to="/"><img className="logo" src="/assets/logoWeb.png"/></Link>
                    </Navbar.Brand>
                    <Navbar.Toggle aria-controls="navbar-toggle" className="border-0" />
                    <Navbar.Collapse className="ml-5" id="navbar-toggle">

                        <NavDropdown title="Products" id="basic-nav-dropdown">
                            <NavDropdown.Item href="/#/article/new">Add an article</NavDropdown.Item>
                            <NavDropdown.Item href="/2">Another action</NavDropdown.Item>
                            <NavDropdown.Item href="/3">Something</NavDropdown.Item>
                        </NavDropdown>
                        <NavDropdown title="Service" id="basic-nav-dropdown">
                            <NavDropdown.Item href="/1">Action</NavDropdown.Item>
                            <NavDropdown.Item href="/2">Another action</NavDropdown.Item>
                            <NavDropdown.Item href="/3">Something</NavDropdown.Item>
                        </NavDropdown>
                        <NavDropdown title="Contact" id="basic-nav-dropdown">
                            <NavDropdown.Item href="/1">Action</NavDropdown.Item>
                            <NavDropdown.Item href="/2">Another action</NavDropdown.Item>
                            <NavDropdown.Item href="/3">Something</NavDropdown.Item>
                        </NavDropdown>
                        <NavDropdown title="Infos" id="basic-nav-dropdown">
                            <NavDropdown.Item href="/1">Action</NavDropdown.Item>
                            <NavDropdown.Item href="/2">Another action</NavDropdown.Item>
                            <NavDropdown.Item href="/3">Something</NavDropdown.Item>
                        </NavDropdown>

                            <input id="search_bar" type='text' onChange={(e)=>{/*appeller une ft qui est dans les props, elle vient de app.js*/}} placeholder=" search product here..." />}
                            <NavDropdown title="Profil" className="edit" id="basic-nav-dropdown">
                                <NavDropdown.Item href="/#/show">My Account</NavDropdown.Item>
                                <NavDropdown.Item href="/#/user/add/credit-card">Payment</NavDropdown.Item>
                                <NavDropdown.Item href="/#/user/edit/adress">Delivery</NavDropdown.Item>
                            </NavDropdown>
                        <Nav className="ml-auto">
                            <Link className="nav-link" to="/login"><IoMdLogIn size={25} color="blue" /></Link>
                            <Link className="nav-link" to="/register"><FaRegRegistered size={22} color="red" /></Link>
                            <Link className="nav-link" to="/cart"><IoIosCart size={25} color="orange" /></Link>
                        </Nav>
                    </Navbar.Collapse>
                </Navbar>
                <div id="message" data-target="false" className="alert alert-success">Nonjour</div>
        </div>
    )
}

export default Navb;
