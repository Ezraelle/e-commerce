import React, { Component } from 'react';
import axios from 'axios';
import "./cart.css";

class Cart extends Component {
    constructor(props){
      super(props);
      this.state = {
          result:"",
          price: 0,
          message: ""
      }
    }

    componentDidMount(){
      this.getInfoArticle()
    }

    getInfoArticle(){
      this.setState({result: '', price:0});
      var key="id_product";
      var ls = localStorage.getItem(key);
      if(ls !== '' && ls !== null) {
        var ids = ls.split(",").sort();
        var lastId = 0;
        var qty = [];
        ids.map((id) => {
          if (lastId !== id ) {
            qty[id] = this.countOccuranceInArray(ids, id);
            lastId = id;
            axios.get(`https://127.0.0.1:8000/article/${lastId}`).then((res) => {
              var articleData = res.data;
              var picPath = "noPic.jpg";
              articleData.picAlt = "no Pic avalable"
              axios.get(`https://127.0.0.1:8000/article/picture/${articleData.id}`).then(res => {
                if (res.data !== undefined && res.data !== null) {
                  articleData = res.data.articleId;
                  picPath = res.data.path;
                  articleData.picAlt = res.data.alt;
                }
                axios.get(`https://127.0.0.1:8000/picture/find/${picPath}`).then(res => {
                  articleData.qty = qty[articleData.id];
                  articleData.picData = res.data;
                  this.setState({
                    price: parseFloat(parseFloat(this.state.price) + (parseFloat(articleData.price) * parseFloat(articleData.qty)))
                  });
                  var result = this.state.result;
                  result = [result, this.setResult(articleData)];
                  this.setState({
                    result: result
                  });
                }).catch(err => {console.log(err)})
              }).catch(err => {console.log(err);})
            }).catch(err => {console.log(err)});
          }
        });
      } else {  // si localStorage est vide
        this.setState({
          result: <p className="text-danger">Your basket is empty </p>
        });
      }
    }

    setResult = (info) => {
      var src =`data:image/jpeg;base64,${info.picData}`
      return (
        <div key={info.id} className="content-article">
          <div className="photo">
            <img src={src} alt={info.picAlt} />
          </div>
          <div className="titre">
            <div>
              <h3>{info.title}</h3>
            </div>
            <div>
              <label>Qty : </label>
              <input data-id={info.id} type="number" defaultValue={info.qty} onBlur={(event) => {this.changeQty(event.target)}}/>
            </div>
            <button id={info.id} onClick={(event) => this.deleteActicleFromBasket(event.target.id, info.qty, () => this.getInfoArticle())} className="btn-sm btn-danger sm">Delete</button>
          </div>
          <div className="prix">{info.price} €</div>
        </div>
      );
    }

    changeQty(target) {
      let diff = target.value - target.defaultValue;
      if (diff < 0)
        this.deleteActicleFromBasket(target.getAttribute('data-id'), Math.abs(diff), () => this.getInfoArticle())
      else if (diff > 0)
        this.addArticleToBasket(target.getAttribute('data-id'), Math.abs(diff), () => this.getInfoArticle())
    }

    displayBasket(){
      return this.state.result;
    }

    myTrim(x) {
      return x.replace(",",'');
    }

    countOccuranceInArray(array, value) {
      let occur = array.filter(arr => arr === value);
      return occur.length;
    }

    addArticleToBasket(id, nb, callback) {
      var key="id_product";
      var array = localStorage.getItem(key).split(',');
      for (var i = 0; i < nb; i++) {
        array.push(id);
      }
      localStorage.setItem(key, array.join());
      if (callback !== undefined)
        callback()
    }

    deleteActicleFromBasket(id, nb, callback){
      for (var i = 0; i < nb; i++) {
        var key="id_product";
        var array = localStorage.getItem(key).split(',');
        if(array.length === 1) {
          localStorage.setItem(key, "");
          this.setState({price : 0});
        } else {
          array.splice(array.indexOf(id), 1)
          localStorage.setItem(key, array.join());
        }
      }
      if (callback !== undefined)
        callback()
    }


    render(){
      return(
        <div className="container">
          <div className="header-article">
            <h2>Your basket of items :</h2>
            <p>Price</p>
          </div>
            <hr></hr>
          {this.displayBasket()}
          <hr></hr>
          <div className="total-article">
            <h4>Total price of items :</h4>
            <p>{this.state.price} €</p>
          </div>
          <div className="footer-article">
            <button className="btn btn-success">Commander</button>
          </div>
        </div>
      )
    }
}

export default Cart;
