import React, { Component } from 'react';
// import axios from 'axios';
class UploadPicture extends Component {
  constructor(props) {
    super(props);
    this.state = {
      addPicture: true
    }
  }

  handleSubmit = (e) => {
    e.preventDefault();
    this.props.function(e.target.picture.files);
  }

  displayPictureInput() {
    return (
      <form onSubmit={(e) => {this.handleSubmit(e)}} encType="multipart/form-data">
        <input type="file" name="picture" accept="image/png, image/jpeg" multiple />
        <input type="submit" name="upload" value="Upload" />
      </form>
    );
  }

  render() {
    return (
      <div>
        {this.displayPictureInput()}
      </div>
    );
  }
}
export default UploadPicture;
