import React, { Component } from 'react';
import data from './data.json';


class Specification extends Component {

  render() {
    var specs = this.props.spec;
    if (specs !== undefined && specs !== null) {
      return(
        <div>
          <div id="spec">
            <ul>
              {specs.map((spec) => {
                let html =
                  <li><hr></hr>{spec.specificationTitle} : {spec.specificationContent}</li>

                return html;
              })}
            </ul>
            <hr></hr>
          </div>
        </div>
        )
    } else {
      return (<div>Specification not found :/</div>)
    }
  }
}

export default Specification;
