import React, { Component } from 'react';
import axios from 'axios';
import { Container, Row, Col, Button } from 'react-bootstrap';
import { IoIosStar, IoIosStarOutline } from "react-icons/io";
import { MdEuroSymbol } from "react-icons/md";
import Specification from './Specification.js'

class ShowArticle extends Component {

    state = {
        articles:{},
        picData: "",
        picAlt:""
    }

    componentDidMount(){
      console.log('ShowArticle');
        this.ShowArticleDetailsHandler()
    };

    ShowArticleDetailsHandler(){
        let url = window.location.href.substring(window.location.href.lastIndexOf('/'));
        let id = url.replace('/','');
        axios.get(`https://127.0.0.1:8000/article/${id}`)
          .then(res => {
            var article = res.data;
            axios.get(`https://127.0.0.1:8000/article/picture/${article.id}`).then(res => {
              if (res.data === null) {
                var picPath = "noPic.jpg"
                var picAlt = "no picture avalable";
              } else {
                var picPath = res.data.path;
                var picAlt = res.data.alt;
              }
              axios.get(`https://127.0.0.1:8000/picture/find/${picPath}`).then(res => {
                this.setState({articles: article, picData: res.data, picAlt: picAlt})
              }).catch(err => {console.log(err);})
            })
            .catch( err => {
                console.log(err);
            })
          })
          .catch( err => {
              console.log(err);
          } );
    }
    render(){
      var src =`data:image/jpeg;base64,${this.state.picData}`;
        return(
            <Container fluid={true}>
                <Row className="vh-50 justify-content-center">
                    <Col xl={4} lg={6} md={6} sm={6} className="">
                        <div className="mt-5 mr-5 mx-auto align-middle" >
                            <img src={src} alt={this.state.picAlt}  id="img_articles"/>
                        </div>
                    </Col>
                    <Col xl={4} lg={6} md={6} sm={6} className="">
                        <div className="mt-5">
                            <h1 className="text-secondary">{this.state.articles.title}</h1>
                            <IoIosStar color="orange" /><IoIosStar color="orange" /><IoIosStar color="orange" /><IoIosStar color="orange" /><IoIosStarOutline color="orange" />
                            <span className="d-inline align-middle ml-3 text-info">lire les avis</span>
                            <hr></hr>
                        </div>
                        <div className="mt-3">
                            <h1 className="d-inline align-middle" style={{'color':'red'}}>{this.state.articles.price}</h1>
                            <span className="align-middle"><MdEuroSymbol size={50} color="red"/></span>
                        </div>
                        <div className="m-0 p-0">
                            <small className="m-0 p-0">TTC</small>
                            <hr></hr>
                        </div>
                        <div>
                            <Button variant="danger" size="lg">Ajouter au panier</Button>
                            <hr></hr>
                        </div>
                        <div className="mt-1 m-0">
                            <p className="text-success font-weight-bold">Livraison en 48h</p>
                            <p >Stock : {this.state.articles.stock}</p>
                            <hr></hr>
                        </div>

                    </Col>
                </Row>
                <Row className="vh-50 justify-content-center p-5">
                    <Col xl={4} lg={4} md={4} sm={3}>
                        <div className="mt-5 text-justify">
                            <p className="text-secondary font-weight-bold">Description</p>
                        </div>
                        <div >
                            <p className="text-secondary m-0 p-0">{this.state.articles.description}</p>
                        </div>
                    </Col>
                    <Col xl={4} lg={4} md={4} sm={3}>
                      <div className="mt-5 text-center">
                        <p className="text-secondary font-weight-bold">Spécifications</p>
                      </div>
                      <div>
                      <p className="text-secondary m-0 p-0"><Specification spec={this.state.articles.specification}/></p>
                      </div>
                    </Col>
                    <Col xl={4} lg={4} md={4} sm={""}><div className="mt-5 text-center"><p className="text-secondary font-weight-bold">Avis</p></div></Col>
                </Row>
            </Container>
        )
    }
}

export default ShowArticle;
