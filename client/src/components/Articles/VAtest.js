import React, { Component } from 'react';
import axios from 'axios';
import "./cart.css";

class Cart extends Component {
    constructor(props){
        super(props);
        this.state = {
            result:"",
            price: 0,
            basket: [],
            message: "",
            image:""
        }
        console.log(this.state.image)
    }
    
    componentDidMount(){
        {this.getInfoArticle()}
    }

    getInfoArticle(){
        var key="id_product";
        var ids = localStorage.getItem(key);
        console.log('getInfoArticle', ids)
        if(ids !== '' && ids !== null){
            this.setState({result: '', price:0});
            var show = ids.split(",");       
            show.map((data) => {
                axios.get(`https://127.0.0.1:8000/article/${data}`).then((res) => {
                    var articleInfo = res.data;
                    axios.get(`https://127.0.0.1:8000/article/picture/${articleInfo.id}`).then(res => {
                        // this.setState({
                        //     image:res.data.path
                        // })
                        console.log(res.data.path)
                        var src ="image/article/" + res.data.path;
                        result = [result,
                            <div className="content-article">
                                <div className="photo">
                                    <img className="imageProduct" src={src} alr="imatge article"/>
                                </div>
                                <div className="titre">
                                    <div>
                                        <h3>{articleInfo.title}</h3>
                                    </div>
                                    <div>
                                        <label>Qty</label>
                                        <select>
                                            <option>1</option>
                                            <option>2</option>
                                            <option>3</option>
                                        </select>
                                    </div>
                                        <button id={articleInfo.id} onClick={(event) => this.deleteActicleFromBasket(event)} className="btn-sm btn-danger sm">Delete</button>
                                </div>
                                <div className="prix">{articleInfo.price} €</div>
                            </div>]
                            this.setState({
                                result: result
                            })
                    })
                    .catch( err => {
                        console.log(err); 
                    })
                    var result = this.state.result
                    this.setState({
                        price: parseInt(parseInt(this.state.price, 10) + parseInt(res.data.price, 10), 10)
                    })

                  
                })
            })
        }else{
            this.setState({
                result: <p className="text-danger">Your basket is empty </p>
            })
        }
    }
         
    displayBasket(){
        return this.state.result;
    }

    myTrim(x) {
        return x.replace(",",'');
      }
      


    deleteActicleFromBasket(event){
        var key="id_product";
        var basket = localStorage.getItem(key);
        var id = event.target.id
        var array = basket.split(',')
        var index = array.indexOf(id)
        if( array.length == 1){
            localStorage.setItem(key, "");
            this.setState({
                basket:"", price:0
            })
            this.getInfoArticle()
        }
        else{
            array.splice(index, 1)
            basket = this.myTrim(basket)
            basket = array.join()
            localStorage.setItem(key, basket);
            this.setState({
                basket: basket
            })
            {this.getInfoArticle()}
        }
    }


    render(){
        return(
            <div className="container">
                <div className="header-article">
                    <h2>Your basket of items :</h2>
                    <p>Price</p>
                </div>
                    <hr></hr>
                {this.displayBasket()}
                <hr></hr>
                <div className="total-article">
                    <h4>Total price of items :</h4>
                    <p>{this.state.price} €</p>
                </div>
                <div className="footer-article">
                    <button className="btn btn-success">Commander</button>
                </div>
            </div>
        )
    }
}

export default Cart;
