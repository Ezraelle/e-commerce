import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { MdEuroSymbol } from 'react-icons/md';
import axios from 'axios';

class VignetteArticle extends Component {
  constructor(props) {
    super(props);
    this.state = {
      articles:[]
    }
  }
    componentDidMount() {
      axios.get('https://127.0.0.1:8000/article').then(res => {
          res.data.map((article) => {
            axios.get(`https://127.0.0.1:8000/article/picture/${article.id}`).then(res => {
              if (res.data === null) {
                var picPath = "noPic.jpg"
                var picAlt = "no picture avalable";
              } else {
                var picPath = res.data.path;
                var picAlt = res.data.alt;
              }
              axios.get(`https://127.0.0.1:8000/picture/find/${picPath}`).then(res => {
                article.picData = res.data;
                article.picAlt = picAlt;
                articles.push(article);
                this.setState({articles: articles})
              }).catch(err => {console.log(err);})
              var articles = this.state.articles;
            })
            .catch( err => {
                console.log(err);
            })
          })
      })
      .catch( err => {
          console.log(err);
      });
    }

    render() {
        return(
            <div className=" row d-flex mt-5">
                {this.state.articles.map(article => {
                    if (article !== null) {
                      var src =`data:image/jpeg;base64,${article.picData}`
                      return(
                        <div className="card" key={article.id}>
                        <img src={src} style={{width: "100%"}} alt={article.picAlt}></img>
                        <Link to={`/article/${article.id}`}><h1>{article.title}</h1></Link>
                        <p className="price">{article.price}€</p>
                        <p>{article.description}</p>
                        <p><button onClick={() => {this.props.addProduct(article.id)}}>Add to Basket</button></p>
                        </div>
                      );
                    }
                })}

            </div>
        )
    }
}

export default VignetteArticle;
