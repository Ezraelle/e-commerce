import React, { Component } from 'react';
import axios from 'axios';
import UploadPicture from './UploadPicture';
import "./Article.css";

class ArticleNewBetter extends Component {
  constructor(props) {
    super(props);
    this.state = {
      displayMainForm: true,
      correctMainFormData: false,
      title: '',
      description: '',
      price: '',
      html_description: '',
      stock: '',
      specificationTitle: '',
      specificationContent: '',
      specifications: [],
      picture: [],
      data: {}
    }
  }

  handleBlurMainForm(e) {
    if (e.target.value === "") {
      console.log(e.target.name);
      e.target.style.borderColor = "red";
      this.setState({correctMainFormData: false});
    } else {
      e.target.style.borderColor = "";
      this.setState({correctMainFormData: true});
    }
    var key = e.target.name
    this.setState({[key]: e.target.value});
  }

  handleBlurSpec(e) {
    let key = e.target.name
    this.setState({[key]: e.target.value});
  }

  handleAddSpec() {
    let spec = this.state.specifications;
    let newSpec = {specificationTitle: this.state.specificationTitle, specificationContent: this.state.specificationContent};
    spec.push(newSpec);
    this.setState({specifications: spec});
  }

  handleSubmitMain(e) {
    e.preventDefault();
    let data = {title: this.state.title,
                description: this.state.description,
                price: this.state.price,
                html_description: this.state.html_description,
                stock: this.state.stock,
                specifications: this.state.specifications};
    let formData = new FormData();
    let picture = this.state.picture;
    for (var [key, value] of Object.entries(data)) {
      if (key === 'specifications') value = JSON.stringify(value);
      formData.append(key, value);
    }
    if (picture !== null && picture !== undefined) {
      for (const [key, value] of Object.entries(picture)) {
        formData.append('key', value);
      }
    }
    this.sendData(formData);
  }

  saveFiles = (data) => {
    console.log('SAVEfILE');
    this.setState({picture: data});
  }

  sendData(data) {
    // let data = this.makeFormData();
    axios.post('https://127.0.0.1:8000/article/new', data, {
      headers: {
        'accept' : "application/json",
        'Content-Type': 'multipart/form-data'
      }
    }).then(res => {
        console.log(res.data);
    })
    .catch( err => {
        console.log(err);
    })
  }


  displayMainForm = () => {
    if (this.state.displayMainForm) {
      let spec = this.state.specifications;
      let form =
      <div className="container">
            <h1> Add Product </h1>
            <div className="aboutadd mx-auto">
            <UploadPicture
              function={this.saveFiles}
              />
              <form action="#" className="form" method="POST" onSubmit={(e) => {this.handleSubmitMain(e);}}>
                <fieldset>
                  <div class="form-group">
                      <label className="ml-5 mr-2">Title:*
                      <input onBlur={(e) => {this.handleBlurMainForm(e)}}
                      name="title" type="text"
                      placeholder="article title"
                      defaultValue={this.state.data.title}
                      className="form-control"/>
                      </label>
                  </div>
                  <div class="form-group">
                      <label className="ml-5 mr-2">Description:*
                      <textarea onBlur={(e) => {this.handleBlurMainForm(e)}}
                      name="description"
                      rows="5"
                      className="form-control"
                      cols="33"
                      placeholder="Article description..."
                      defaultValue={(this.state.data.description === undefined) ? "Article description..." : this.state.data.description}/>
                      </label>
                  </div>
                  <div class="form-group">
                      <label className="ml-5 mr-2">Price:*
                      <input onBlur={(e) => {this.handleBlurMainForm(e)}}
                      name="price"
                      type="number"
                      placeholder="price"
                      defaultValue={this.state.data.price}
                      step="0.01" min="0"
                      className="form-control"/>

                      </label>
                  </div>
                  <div class="form-group">
                      <label className="ml-5 mr-2">Html:*
                      <input onBlur={(e) => {this.handleBlurMainForm(e)}}
                      name="html_description"
                      type="text"
                      placeholder="Html description"
                      defaultValue={this.state.data.html_description}
                      className="form-control"/>
                      </label>
                  </div>
                  <div class="form-group">
                      <label className="ml-5 mr-2">Stock:*

                      <input onBlur={(e) => {this.handleBlurMainForm(e)}}
                      name="stock"
                      type="number"
                      placeholder="stock"
                      defaultValue={this.state.data.stock}
                      className="form-control"
                      min="0"/>
                      </label>
                  </div>
                  {
                    spec.map((s) => {
                      return <p><span>{s.specificationTitle} : </span>{s.specificationContent}</p>
                    })
                  }
                  <input onBlur={(e) => this.handleBlurSpec(e)} name="specificationTitle" type="text" placeholder="new specification title" />
                  <input onBlur={(e) => this.handleBlurSpec(e)} name="specificationContent" type="text" placeholder="new specification content" />
                  <button onClick={() => {this.handleAddSpec()}} name="add" type="button"> Add </button>
                  {/*this.displaySpecificationForm()*/}
                  <input className="btn btn-success ml-5" name="save" type="submit" value="Save"/>
                </fieldset>
              </form>
            </div>
      </div>
      return form;
    }
  }

  render() {
    return (
      <div>
        {this.displayMainForm()}
      </div>
    );
  }
}
export default ArticleNewBetter;
