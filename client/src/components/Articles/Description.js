import React, { Component } from 'react';

class Description extends Component {

  createHtml() {
    return {__html: this.props.file};
  }

  render() {
    return(
      <div>
        <div dangerouslySetInnerHTML={this.createHtml()} />
      </div>
    )
  }
}

export default Description;
