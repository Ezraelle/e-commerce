import React from 'react';
// import ReactDOM from 'react-dom';
import { Redirect } from 'react-router'
import axios from 'axios';
import EditUserInfos from './userEdit';
import EditUserAdress from './userAdress';
import "./userStyle.css";


class DisplayUserInfos extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      user: {},
      modifier: false,
      redirection: false
    }
    this.getUserInfo()
  }

  // *** *** ***  *** *** *** *** *** *** *** *** *** //
  // Enlever le 1 et le remplacer par le vrai { id }  //
  // *** *** ***  *** *** *** *** *** *** *** *** *** //

  getUserInfo() {
    axios.get(`https://localhost:8000/user/1`)
      .then(res => {
        this.setState({user: res.data});
      });
  }

  displayInfos(){
    if(!this.state.modifier) {
      var allInfos =
        <div className="container">
            <h1>About you !</h1>
          <div className="about col-6 mx-auto">
            <ul>
              <li><b>Firstname:</b>  {this.state.user.firstname}</li>
              <li><b>Lastname:</b> {this.state.user.lastname}</li>
              <li><b>Email:</b>  {this.state.user.email}</li>
              <li><b>Phone:</b> { this.state.user.phone}</li>
            </ul>
            <button onClick={() => this.change()} className="btn btn-success ml-4"> Edit </button>
            <button onClick={() => this.delete()} className="btn btn-danger ml-4"> Delete account</button>
          <div className="add">
            <a href="#/user/add/credit-card">Add a credit card</a>
            <a href="#/user/edit/adress">Add a delivery adress</a>
          </div>
          </div>
         </div>
      return allInfos;
    }
    else if (this.state.modifier) {
      return (
        < EditUserInfos
        change={() => this.change()}
        infos= {this.state.user}
        />
      );
    }
  }

  change() {
    this.state.modifier ? this.setState({modifier:false}) : this.setState({modifier:true})
    this.getUserInfo()
  }

  delete(){
    axios.delete(`https://localhost:8000/user/1`)
      .then(res => {
        this.setState({ redirection: true })
        console.log(res)
      });

  }

  render() {
    const { redirection } = this.state;
    if (redirection) {
     return <Redirect to='/'/>;
    }
    else{
      return (
        <div>
          {this.displayInfos()}
        </div>
      );
    }
  }
}
export default DisplayUserInfos;
