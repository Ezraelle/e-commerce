import React, { Component } from 'react';
// import axios from 'axios';
import 'bootstrap/dist/css/bootstrap.min.css';

class UserInfosCb extends Component {
    constructor(props) {
        super(props);
        this.state = {
            card_number: "",
            expiration_date: "",
            error:""

        }
    }

    handleSubmit(event){
        event.preventDefault();
        this.setState({
            card_number: event.target.card.value,
            expiration_date: event.target.expiration.value,

        })
    }

    handleChange(event){
        if(event.target.type === "number"){
            let cardlength = event.target.value.length;
            if(cardlength < 16){
                this.setState({
                    card_number: event.target.value
                })
            }
            else{
                event.target.value = this.state.card_number
            }
        }
        else if(event.target.type === "month"){
            var date = new Date();
            var cardDate = new Date(event.target.value);
            var diffDate=(cardDate - date)
            if(diffDate < 2628000){
                this.setState({
                    error: "Expiration date invalid."
                })
            }else{
                this.setState({
                    error: ""
                })
            }
        }
    }

    render(){
        return (
            <div className="container">

                <h2>Add a credit card</h2>
                <div className="about col-6 mx-auto">
                    <form onSubmit={(event) => this.handleSubmit(event)} className="form">
                        <div className="form-group">
                            <label>
                                Card number:
                                <input className="form-control"
                                name="card"
                                type="number"
                                onChange={(event) => this.handleChange(event)}
                                />
                            </label>
                        </div>
                        <div className="form-group">
                            <label>
                                Card's owner:
                                <input className="form-control"
                                type="text"
                                />
                            </label>
                        </div>
                        <div className="form-group">
                            <label>
                                Expiration date:
                                <input className="form-control"
                                name="expiration"
                                type="month"
                                onChange={(event) => this.handleChange(event)}
                                />
                            </label>
                            <p>{this.state.error}</p>
                        </div>
                        <input className="btn btn-primary"
                        type="submit"
                        value="Envoyer"
                        />
                    </form>
                </div>

            </div>
        );
    }
}
export default UserInfosCb;
