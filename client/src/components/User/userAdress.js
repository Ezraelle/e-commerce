import React, { Component } from 'react';
// import axios from 'axios';
import 'bootstrap/dist/css/bootstrap.min.css';

class EditUserAdress extends Component {
    constructor(props) {
        super(props);
        this.state = {
            adress: "",
            city: "",
            post_code: "",
            country: ""
        }
    }

    handleSubmit(event){
        event.preventDefault();
        this.setState({
            adress: event.target.adress.value,
            city: event.target.city.value,
            post_code: event.target.post_code.value,
            country: event.target.country.value
        })
    }

    render(){
        return (
            <div>
                <div className="container">
                <h1>Complete your personal infos</h1>
                  <div className="about col-6 mx-auto">
                    <form onSubmit={(event) => this.handleSubmit(event)} className="form">
                        <div className="form-group">
                            <label>
                                Adress:
                                <input className="form-control"
                                name="adress"
                                type="text"
                                />
                            </label>
                        </div>
                        <div className="form-group">
                            <label>
                                City:
                                <input className="form-control"
                                name="city"
                                type="text"
                                />
                            </label>
                        </div>
                        <div className="form-group">
                            <label>
                                Post code:
                                <input className="form-control"
                                name="postcode"
                                type="number"
                                minLength="5"
                                maxLength="7"
                                />
                            </label>
                        </div>
                        <div className="form-group">
                            <label>
                                Country:
                                <input className="form-control"
                                name="country"
                                type="text"/>
                            </label>
                        </div>
                        <input className="btn btn-primary"
                        type="submit"
                        value="Envoyer"
                        />
                    </form>


                  </div>
                </div>
            </div>
        );
    }
}
export default EditUserAdress;
