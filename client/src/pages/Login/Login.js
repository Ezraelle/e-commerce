import React, { Component } from 'react';
import { Redirect } from 'react-router';
import { Container, Row, Col, Form, Button } from 'react-bootstrap';
import axios from 'axios';

class Login extends Component {
    state = {
        user: {
            email:'',
            password:''
        },
        errors: {},
        redirect: false
     }
    // Handle Input when typing
    handleInputChange = (e) => {
        const input = e.target.name;
        const user = this.state.user;
        user[input] = e.target.value
        console.log(e.target.value)
        this.setState({ user });
    }

    handleFormSubmit = (e) => {
        e.preventDefault();
        let formData = new FormData();
        formData.append('email', this.state.user.email);
        formData.append('password', this.state.user.password);
        // const email = this.state.user.email;
        // const password = this.state.user.password;
        // const formData = { email, password };
        //this.setState({ redirect:true })

        axios.post('https://127.0.0.1:8000/login', formData, { headers: { 'Accept': 'application/json'} })
            .then(( response ) => {
              console.log(response.data);
                // this.setState({ errros: {}, redirect: true })
                // axios.get('https://127.0.0.1:8000/test').then((res) => {console.log(res.data);})
            })
            .catch( err => console.log(err));
    }

    render() {
        const { redirect } = this.state;
        if (redirect) {
            return <Redirect to='/home' />
        }
        return (
            <Container fluid={true}>
                <Row className="vh-100 justify-content-center align-items-center">
                    <Col xl={6} lg={6}>
                    <h1 className="text-primary">LOGIN</h1>
                        <Form onSubmit={this.handleFormSubmit}>
                            <Form.Group controlId="formBasicEmail">
                                <Form.Label>Email address</Form.Label>
                                <Form.Control onChange={this.handleInputChange} name="email" type="email" placeholder="Enter email" />
                                <Form.Text className="text-muted">
                                </Form.Text>
                            </Form.Group>

                            <Form.Group controlId="formBasicPassword">
                                <Form.Label>Password</Form.Label>
                                <Form.Control onChange={this.handleInputChange} name="password" type="password" placeholder="Password" />
                            </Form.Group>
                            <Button variant="primary" type="submit">
                                Login
                            </Button>
                        </Form>
                    </Col>
                </Row>
            </Container>
         );
    }
}

export default Login;
