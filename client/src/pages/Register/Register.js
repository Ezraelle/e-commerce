import React, { Component } from 'react';
import { Container, Row, Col, Form, Button } from 'react-bootstrap';
import { Redirect } from 'react-router';
import axios from 'axios';

class Register extends Component {

    state = {
        user: {
            lastname:'',
            firstname:'',
            phone:'',
            email:'',
            password:''
        },
        successMsg:'',
        formError:{
            lastname:'',
            firstname:'',
            email:'',
            phone:'',
            password:'',
            formEmpty:''
        },
        redirect:false
    }

    handleInputChange = (e) => {
        const input = e.target.name;
        const user = this.state.user;
        user[input] = e.target.value;
        // let formErrors = { ...this.state.formErrors };
        this.setState({ user });
    }

    handleFormSubmit = (e) => {
        e.preventDefault();
        // console.log('submitted')
        const lastname = this.state.user.lastname;
        const firstname = this.state.user.firstname;
        const phone = this.state.user.phone;
        const email = this.state.user.email;
        const password = this.state.user.password;
        const formData = { lastname, firstname, phone, email, password };

        axios({
            url: 'https://127.0.0.1:8000/register',
            method: 'POST',
            headers: {
                "Content-Type": "multipart/form-data",
            },
            data: formData
        }).then(res => {
            console.log(res)
            this.setState({ successMsg: res.data.msg, redirect: true })
        }).catch(err => {
            console.log(err)
        })
    }

    render(){
        const { redirect } = this.state;
        if (redirect) {
            return <Redirect to='/login' />
        }
        return(
            <Container fluid={true}>
                <Row className="vh-100 justify-content-center align-items-center">
                    <Col xl={6} lg={6}>
                        <h1 className="text-danger">REGISTER</h1>
                        <span>{this.state.successMsg}</span>
                        <Form onSubmit={this.handleFormSubmit}>
                            <Form.Group>
                                <Form.Label>Lastname</Form.Label>
                                <Form.Control onChange={this.handleInputChange}  name="lastname" type="text" placeholder="Enter your lastname" />
                                <Form.Text className="text-muted">
                                </Form.Text>
                            </Form.Group>
                            <Form.Group>
                                <Form.Label>Firstname</Form.Label>
                                <Form.Control onChange={this.handleInputChange}  name="firstname" type="text" placeholder="Enter your firstname" />
                                <Form.Text className="text-muted">
                                </Form.Text>
                            </Form.Group>
                            <Form.Group>
                                <Form.Label>Phone Number</Form.Label>
                                <Form.Control onChange={this.handleInputChange}  name="phone" type="text" placeholder="Enter your phone number" />
                                <Form.Text className="text-muted">
                                </Form.Text>
                            </Form.Group>
                            <Form.Group>
                                <Form.Label>Email address</Form.Label>
                                <Form.Control onChange={this.handleInputChange}  name="email" type="email" placeholder="Enter email" />
                                <Form.Text className="text-muted">
                                </Form.Text>
                            </Form.Group>

                            <Form.Group controlId="formBasicPassword">
                                <Form.Label>Password</Form.Label>
                                <Form.Control onChange={this.handleInputChange}  name="password" type="password" placeholder="Enter Password" />
                            </Form.Group>
                            <Button variant="danger" type="submit">
                                Register
                            </Button>
                        </Form>
                    </Col>
                </Row>
            </Container>
        )
    }
}
export default Register;
