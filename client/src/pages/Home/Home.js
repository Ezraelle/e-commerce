import React from 'react';

const Home = () => {
    return(
        <div className="col-8 container mt-5">
            <h1>Welcome to the home page</h1>
            <br/>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Fugiat atque tempore nihil odio voluptate quidem cumque voluptatum alias eaque autem eos nisi ad nemo, beatae quod debitis sit numquam aut.</p>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Fugiat atque tempore nihil odio voluptate quidem cumque voluptatum alias eaque autem eos nisi ad nemo, beatae quod debitis sit numquam aut.</p>
            
        </div>
    )
}
export default Home;