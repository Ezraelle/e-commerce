import React, { Component } from 'react';
import axios from 'axios';
import { HashRouter, Route, Switch } from 'react-router-dom';
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
// import Navbar from './components/Navbar/Navbar';
import ShowArticle from './components/Articles/ShowArticle.js';
import VignetteArticle from './components/Articles/VignetteArticle.js';
// import Specification from './components/Articles/Specification.js'
import { Container } from 'react-bootstrap'
import Navb from './components/Navbar/Navb';
import UserShow from './components/User/DisplayUserInfos';
import UserInfosCb from './components/User/UserInfosCb';
import EditUserAdress from './components/User/userAdress';
import Home from './pages/Home/Home';
import Login from './pages/Login/Login';
import Register from './pages/Register/Register';
import ArticleNew from './components/Articles/ArticleNew'
import Cart from './components/Cart/DisplayCart'

class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
        articles:[]
        }
    }

    /*
    set un state avec la valeur actuelle de la recherche,
    passer cette valeur à VignetteArticle,
    dans VignetteArticle : modifier la requet axios pr qu'elle
    appelle ta ft dans symfony
    */

    addProductToBasket= (id) => {
        var key="id_product";
        var basket = localStorage.getItem(key);
        if(basket !== null && basket !== ""){
            basket = basket + ',' + id;
        } else {
            basket = id
        }
        localStorage.setItem(key, basket);
    }

    render(){
        return (
          <HashRouter>
               <Navb />

               <Container fluid={true}>
                <Switch>
                   <Route
                       path="/"
                       exact
                       render={ () => <VignetteArticle addProduct={this.addProductToBasket}/> }
                   />
                   <Route
                       path="/article/new"
                       render={ () => <ArticleNew /> }
                   />
                    <Route
                        path="/article/:id"
                        render={ () => <ShowArticle /> }
                    />
                   <Route
                       path="/login"
                       exact
                       render={ () => <Login /> }
                   />
                   <Route
                       path="/register"
                       exact
                       render={ () => <Register /> }
                   />
                   <Route
                       path="/show"
                       exact
                       render={ () => <UserShow /> }
                   />
                   <Route
                       path="/user/edit/adress"
                       exact
                       render={ () =>  <EditUserAdress /> }
                   />
                   <Route
                       path="/user/add/credit-card"
                       exact
                       render={ () =>  <UserInfosCb /> }
                   />
                       <Route
                        path="/cart"
                        exact
                        render={ () => <Cart basket={this.state.basket} /> }
                    />
                    </Switch>
               </Container>

           </HashRouter>
        );
    }
}

export default App;
