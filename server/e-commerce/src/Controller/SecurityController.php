<?php

namespace App\Controller;

use App\Entity\User;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class SecurityController extends AbstractController
{

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->em = $entityManager;
    }
    /**
     * @Route("/login", name="app_login")
     */
    public function login(Request $request, AuthenticationUtils $authenticationUtils, UserRepository $userRepository): Response
    {
        // si l'utilisateur est connecter (donc connexion réussi)
        if ($user = $this->getUser()) {
          $msg = [$user->getRoles(), $user->getEmail()];
        } else {
          // sinon, on recuper error, le login qu'il à essayer et on renvoie sur form
          $error = $authenticationUtils->getLastAuthenticationError();
          $lastUsername = $authenticationUtils->getLastUsername();
          $msg = ['success' => ['error' => $error, "lastUsername" => $lastUsername]];
        }

        $response = new JsonResponse($msg);
        $response->headers->set('Content-Type', 'application-json');
        $response->headers->set('Access-Control-Allow-Origin', '*');
        $response->headers->set('Access-Control-Allow-Methods', 'POST, PUT');
        return $response;
    }

    /**
     * @Route("/logout", name="app_logout")
     */
    public function logout()
    {
        throw new \Exception('This method can be blank - it will be intercepted by the logout key on your firewall');
    }

    /**
     * @Route("/register", name="register")
     */
    public function register(Request $request, UserPasswordEncoderInterface $passwordEncoder)
    {
        $data = json_decode($request->getContent(), true);
        if (!empty($data)) {

            $lastname = (string) $data['lastname'];
            $firstname = (string) $data['firstname'];
            $phone = (string) $data['phone'];
            $email = (string) $data['email'];
            $pwd = (string) $data['password'];

            $user = new User();
            $password = $passwordEncoder->encodePassword($user, $pwd);
            $user->setLastname($lastname);
            $user->setFirstname($firstname);
            $user->setEmail($email);
            $user->setPhone($phone);
            $user->setRoles(array('ROLE_USER'));
            $user->setIsOnline(1);
            $user->setPassword($password);
            $em = $this->em;
            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();

            $msg = ['msg' => 'Account created Successfuly'];
            $response = new JsonResponse($msg);
            $response->headers->set('Content-Type', 'application-json');
            $response->headers->set('Access-Control-Allow-Origin', '*');
            $response->headers->set('Access-Control-Allow-Methods', 'POST, PUT');
            return $response;
        } else {
            $msg = ['msg' => 'Sorry, some errors occurs !'];
            $response = new JsonResponse($msg);
            $response->headers->set('Content-Type', 'application-json');
            $response->headers->set('Access-Control-Allow-Origin', '*');
            $response->headers->set('Access-Control-Allow-Methods', 'POST, PUT');
            return $response;
        }
    }
}
