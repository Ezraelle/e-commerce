<?php

namespace App\Controller;

use App\Entity\Article;
use App\Entity\Picture;
use App\Form\ArticleType;
use App\Repository\PictureRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

/**
 * @Route("/picture")
 */
class PictureController extends AbstractController
{
    /**
     * @Route("/get/{articleId}", name="picture_get", methods={"GET"})
     */
    public function show($articleId, Picture $picture, PictureRepository $pictureRepository): Response
    {
        // dd($articleId);
        $encoders = [new XmlEncoder(), new JsonEncoder()];
        $normalizers = [new ObjectNormalizer()];
        $serializer = new Serializer($normalizers, $encoders);
        $all_picture = $pictureRepository->findByArticleId($id);
        $jsonContent = $serializer->serialize($all_picture, 'json');

        $picture = new Response($jsonContent);
        $picture->headers->set('Content-Type', 'application-json');
        $picture->headers->set('Access-Control-Allow-Origin', '*');
        $picture->headers->set('Access-Control-Allow-Methods', 'POST, GET, PUT, OPTIONS');
        return $picture;
    }

    /**
     * @Route("/find/{name}", name="picture_name", methods={"GET"})
     */
    public function findPicture($name): Response
    {
        $file = fopen('image/article/' . $name, 'rb');
        $str = base64_encode(stream_get_contents($file));
        // return $file;
        $picture = new Response($str);
        $picture->headers->set('Content-Type', 'image/jpg');
        $picture->headers->set('Access-Control-Allow-Origin', '*');
        $picture->headers->set('Access-Control-Allow-Methods', 'POST, GET, PUT, OPTIONS');
        return $picture;
    }

    // /**
    //  * @Route("/new", name="article_new", methods={"GET","POST"})
    //  */
    // public function new(Request $request): Response
    // {
    //   $article = new Article();
    //   $article->setTitle($request->request->get('title'));
    //   $article->setDescription($request->request->get('description'));
    //   $article->setPrice($request->request->get('price'));
    //   $article->setSpecification(json_decode($request->request->get('specification')));
    //   $article->setHtmlDescription($request->request->get('html_description'));
    //   $article->setStock($request->request->get('stock'));
    //   $em = $this->getDoctrine()->getManager();
    //   $pictureFile = $request->files->get('key');

    //   if ($pictureFile) {
    //     $originalFilename = pathinfo($pictureFile->getClientOriginalName(), PATHINFO_FILENAME);
    //     $safeFilename = transliterator_transliterate('Any-Latin; Latin-ASCII; [^A-Za-z0-9_] remove; Lower()', $originalFilename);
    //     $newFilename = $safeFilename . '-' . uniqid() . '.' . $pictureFile->guessExtension();
    //     try {
    //       $pictureFile->move('image/article', $newFilename);
    //     } catch (FileExeption $e) {
    //       $msg = ['success' => $e];
    //     }
    //     $picture = new Picture();
    //     $picture->setArticleId($article);
    //     $picture->setPath($newFilename);
    //     $em->persist($picture);
    //   }
    //    $em->persist($article);

    //    if ($em->flush() == null) $msg = ['success' => 'Article Saved'];
    //    else $msg = ['success' => 'An error occured'];
    //    $ret = new Response(json_encode($msg));
    //    $ret->headers->set('Content-Type', 'application/json');
    //    $ret->headers->set('Access-Control-Allow-Origin', '*');
    //    $ret->headers->set('Access-Control-Allow-Methods', 'POST, GET, PUT, OPTIONS');
    //    return $ret;
    // }

    // /**
    //  * @Route("/{id}", name="article_show", methods={"GET"})
    //  */
    // public function show($id, Article $article, ArticleRepository $articleRepository): Response
    // {

    //     $encoders = [new XmlEncoder(), new JsonEncoder()];
    //     $normalizers = [new ObjectNormalizer()];

    //     $serializer = new Serializer($normalizers, $encoders);
    //     $article = $articleRepository->find($id);
    //     $jsonContent = $serializer->serialize($article, 'json');
    //     // dd($jsonContent);
    //     // $jsonContent = "articles";
    //     $article = new Response($jsonContent);
    //     $article->headers->set('Content-Type', 'application-json');
    //     $article->headers->set('Access-Control-Allow-Origin', '*');
    //     $article->headers->set('Access-Control-Allow-Methods', 'POST, GET, PUT, OPTIONS');
    //     return $article;
    // }

    // /**
    //  * @Route("/{id}/edit", name="article_edit", methods={"GET","POST"})
    //  */
    // public function edit(Request $request, Article $article): Response
    // {
    //     $form = $this->createForm(ArticleType::class, $article);
    //     $form->handleRequest($request);

    //     if ($form->isSubmitted() && $form->isValid()) {
    //         $this->getDoctrine()->getManager()->flush();

    //         return $this->redirectToRoute('article_index');
    //     }

    //     return $this->render('article/edit.html.twig', [
    //         'article' => $article,
    //         'form' => $form->createView(),
    //     ]);
    // }

    // /**
    //  * @Route("/{id}", name="article_delete", methods={"DELETE"})
    //  */
    // public function delete(Request $request, Article $article): Response
    // {
    //     if ($this->isCsrfTokenValid('delete'.$article->getId(), $request->request->get('_token'))) {
    //         $entityManager = $this->getDoctrine()->getManager();
    //         $entityManager->remove($article);
    //         $entityManager->flush();
    //     }

    //     return $this->redirectToRoute('article_index');
    // }
}
