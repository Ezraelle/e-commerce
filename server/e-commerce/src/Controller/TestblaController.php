<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class TestblaController extends AbstractController
{
    /**
     * @Route("/test", name="test")
     */
    public function index()
    {
        $ret = new Response(json_encode(
            ["patate" => $this->getUser()->getEmail()]
        ));
        // $ret = new Response(json_encode(
        //     ["patate" => "yes, it is :-)"]
        // ));
        $ret->headers->set('Content-Type', 'application-json');
        $ret->headers->set('Access-Control-Allow-Origin', '*');
        $ret->headers->set('Access-Control-Allow-Methods', '*');
        return $ret;
    }
}
