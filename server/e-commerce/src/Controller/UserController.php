<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\UserType;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

/**
 * @Route("/user")
 */
class UserController extends AbstractController
{
    /**
     * @Route("/", name="user_index", methods={"GET"})
     */
    public function index(UserRepository $userRepository): Response
    {
        return $this->render('user/index.html.twig', [
            'users' => $userRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="user_new", methods={"GET","POST"})
     */
    public function new(Request $request,UserPasswordEncoderInterface $encoder): Response
    {

        $user = new User();
        $form = $this->createForm(UserType::class, $user);
        $form->handleRequest($request);
        
        if ($form->isSubmitted() && $form->isValid()) {
            
            $plainPassword = $user->getPassword();
            $encoded = $encoder->encodePassword($user, $plainPassword);

            $user->setRoles(['ROLE_USER']);
            $user->setIsOnline(false);
            $user->setPassword($encoded);
           
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($user);
            $entityManager->flush();

            return $this->redirectToRoute('user_index');
        }
        return $this->render('user/new.html.twig', [
            'user' => $user,
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/{id}", name="user_show", methods={"GET"})
     */
    public function show(User $user): Response
    {
        $ret = new Response(json_encode(
            [
                "email" => $user->getEmail(),
                "firstname" => $user->getFirstname(),
                "lastname" => $user->getLastname(),
                "phone" => $user->getPhone()                
            ]
        ));
        $ret->headers->set('Content-Type', 'application-json');
        $ret->headers->set('Access-Control-Allow-Origin', '*');
        $ret->headers->set('Access-Control-Allow-Methods', 'POST, GET, PUT, OPTIONS');

        return $ret;
    }

    /**
     * @Route("/{id}/edit", name="user_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, User $user, UserPasswordEncoderInterface $encoder): Response
    {
       
        $user_data = json_decode($request->getContent(), true);

        $plainPassword = $user_data['password'];
        $password = $encoder->encodePassword($user, $plainPassword);

        $user->setFirstname($user_data['firstname']);
        $user->setLastname($user_data['lastname']);
        $user->setEmail($user_data['email']);
        $user->setPhone($user_data['phone']);
        $user->setPassword($password);
        
        $em = $this->getDoctrine()->getManager();
        $em->persist($user);
        
        $em->flush();
    
        $msg = ['success' => 'Article Saved'];
        $ret = new Response(json_encode($msg));
        $ret->headers->set('Content-Type', 'application/json');
        $ret->headers->set('Access-Control-Allow-Origin', '*');
        $ret->headers->set('Access-Control-Allow-Methods', 'POST, GET, PUT, OPTIONS');
        return $ret;
    }

    /**
     * @Route("/{id}", name="user_delete", methods={"DELETE"})
     */
    public function delete(Request $request, User $user): Response
    {
        if ($this->isCsrfTokenValid('delete'.$user->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($user);
            $entityManager->flush();
        }

        $msg = ['success' => 'User deleted'];
        $ret = new Response(json_encode($msg));
        $ret->headers->set('Content-Type', 'application/json');
        $ret->headers->set('Access-Control-Allow-Origin', '*');
        $ret->headers->set('Access-Control-Allow-Methods', 'POST, GET, PUT, OPTIONS');
        return $ret;
    }
}
